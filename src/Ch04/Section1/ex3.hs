import System.Environment (getArgs)

interactWith function inputFile = do
  input <- readFile inputFile
  function input

main = mainWith printFirstWords
  where
    mainWith function = do
      args <- getArgs
      case args of
        [input] -> interactWith function input
        _ -> putStrLn "error: exactly one argument needed"

    printFirstWords text =
      print $ unwords (map firstWord (lines text))
      where
        firstWord = head . words