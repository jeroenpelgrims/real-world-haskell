safeHead :: [a] -> Maybe a
safeTail :: [a] -> Maybe [a]
safeLast :: [a] -> Maybe a
safeInit :: [a] -> Maybe [a]

safeHead [] = Nothing
safeHead (x : _) = Just x

safeTail [] = Nothing
safeTail (_ : xs) = Just xs

safeLast xs | null xs = Nothing
safeLast xs = Just (last xs)

safeInit xs | null xs = Nothing
safeInit xs = Just (init xs)