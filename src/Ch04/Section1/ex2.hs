splitWith :: (a -> Bool) -> [a] -> [[a]]
splitWith _ [] = []
splitWith p xs =
  pre : (splitWith p rest)
  where
    (pre, suf) = break p xs
    rest = if null suf
      then []
      else dropWhile p suf