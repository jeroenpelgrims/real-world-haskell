module Ch04.Section2.Ex5 (groupBy) where

groupBy :: (a -> a -> Bool) -> [a] -> [[a]]
groupBy p xs = foldr step [] xs
  where
    step x [] = [[x]]
    step x (y : rest) | p x (head y) = (x : y) : rest
    step x rest = [x] : rest