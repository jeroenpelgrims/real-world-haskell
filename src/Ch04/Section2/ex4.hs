takeWhile_rec :: (a -> Bool) -> [a] -> [a]
takeWhile_rec p (x : xs) | p x = x : takeWhile_rec p xs
takeWhile_rec _ _ = []

takeWhile_fold :: (a -> Bool) -> [a] -> [a]
takeWhile_fold p xs = foldr step [] xs
  where
    step x acc
      | p x = x : acc
      | otherwise = []