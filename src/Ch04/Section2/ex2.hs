import Data.Char (digitToInt)
import Data.List (isInfixOf)

type ErrorMessage = String

asInt_either :: String -> Either ErrorMessage Int
asInt_either xs | isInfixOf "." xs = Left "not a digit '.'"
asInt_either ('-' : xs) = case asInt_either xs of
  Right x -> Right $ -1 * x
  Left x -> Left x
asInt_either xs = Right $ foldl asInt 0 xs
  where
    asInt acc x = acc * 10 + digitToInt x