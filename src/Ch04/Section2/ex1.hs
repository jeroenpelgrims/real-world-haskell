import Data.Char (digitToInt)
import Data.List (isInfixOf)

asInt xs = loop 0 xs

loop :: Int -> String -> Int
loop acc [] = acc
loop acc (x : xs) =
  let acc' = acc * 10 + digitToInt x
   in loop acc' xs

asInt_fold :: String -> Int
asInt_fold xs | isInfixOf "." xs = error "not a digit '.'"
asInt_fold ('-' : xs) = -1 * asInt_fold xs
asInt_fold xs = foldl asInt 0 xs
  where
    asInt acc x = acc * 10 + digitToInt x
