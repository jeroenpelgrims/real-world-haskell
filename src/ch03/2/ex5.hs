ispalindrome xs | odd (length xs) = False
ispalindrome xs = xs == reverse xs