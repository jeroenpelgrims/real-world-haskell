import Data.List

comp a b =
  case (length a, length b) of
    (la, lb) | la < lb -> LT
    (la, lb) | la > lb -> GT
    otherwise -> EQ

sortlist xs = sortBy comp xs