data Direction = Left | Right | Straight deriving (Show, Eq)

-- formula: https://math.stackexchange.com/questions/274712/calculate-on-which-side-of-a-straight-line-is-a-given-point-located
angle (x1, y1) (x2, y2) (x, y) =
  case d of
    d | d < 0 -> Main.Left
    d | d > 0 -> Main.Right
    otherwise -> Straight
  where d = (x - x1) * (y2 - y1) - (y - y1) * (x2 - x1)

angles (a : b : c : xs) =
  angle a b c : angles (b : c : xs)
angles _ = []