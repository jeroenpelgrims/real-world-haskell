module Main (main) where

import SimpleJSON

main :: IO ()
main = print (JObject [("foo", JNumber 5), ("bar", JBool False)])