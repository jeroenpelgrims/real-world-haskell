import Ch05.SimpleJSON

renderJValue :: JValue -> Doc
renderJValue (JBool False) = text "false"
renderJValue (JBool True) = text "true"
renderJValue JNull = text "null"
renderJValue (JNumber num) = double num
renderJValue (JString s) = string s