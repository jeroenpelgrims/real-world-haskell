module PutJSON where

import Ch05.SimpleJSON
import Data.List (intercalate)

renderJValue :: JValue -> String
renderJValue (JString x) = show x
renderJValue (JNumber x) = show x
renderJValue (JBool True) = "true"
renderJValue (JBool False) = "false"
renderJValue (JObject o) = "{" ++ pairs o ++ "}"
  where
    pairs [] = ""
    pairs ps = intercalate ", " (map pair ps)
    pair (key, value) = show key ++ ":" ++ renderJValue value
renderJValue (JArray x) = "[" ++ values x ++ "]"
  where
    values xs = intercalate ", " (map renderJValue xs)

putJValue :: JValue -> IO ()
putJValue v = putStrLn $ renderJValue v