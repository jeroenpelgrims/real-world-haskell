module Ch04.Section1.Ex5Spec where

import Ch04.Section2.Ex5 as Ex5
import Data.List
import Test.Hspec

spec :: Spec
spec = do
  describe "Ex5::groupBy" $ do
    it "Result should equal Data.List.groupBy" $ do
      let test p xs = actual `shouldBe` expected
            where
              actual = Ex5.groupBy p xs
              expected = Data.List.groupBy p xs
       in do
            test (==) "aaabcccdda"
            test (<) [1, 2, 3, 2, 0, 0, 3, 3, 1, 0]